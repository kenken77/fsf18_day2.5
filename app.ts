console.log("This is typescript !");
//const eventName1: 'click' = "click";
//console.log(typeof eventName1);
const eventName2: "click" = "click";
const eventName3: "click" = null;
const eventName4: "click" = undefined;

console.log(typeof eventName4);

const paypal_uri: string = "http://www.paypal.com";
const eventName5: "" = "";


// const eventName6: "click" = ""; <-- error

//type EventType = "click" | "mouseover";
let eventName: "click" | "mouseover" | "mouseover2" = "mouseover";
console.log(eventName.length);         // 5
console.log(eventName.toUpperCase());  // "CLICK"
console.log(eventName + "!");          // "click!"


eventName = "mouseover";
console.log(eventName.length);         
console.log(eventName.toUpperCase());  
console.log(eventName + "!");          

eventName = "mouseover2";
console.log(eventName.length);         
console.log(eventName.toUpperCase());  
console.log(eventName + "!");          

interface labelledValue {
    
}
